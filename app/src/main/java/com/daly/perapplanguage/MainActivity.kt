package com.daly.perapplanguage

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import com.daly.perapplanguage.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    companion object {
        private const val LOG_TAG = "MainActivity"
        private const val FRENCH_LANGUAGE_QUALIFIER_ANY_REGION = "fr"
        private const val ENGLISH_LANGUAGE_QUALIFIER_ANY_REGION = "en"
    }

    private lateinit var binding: ActivityMainBinding

    private var isFirst = true

    @RequiresApi(33)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initLocalePicker()

        setButtonOnCLick()
    }

    private fun initLocalePicker() {
        val spinner: Spinner = binding.localePicker
        spinner.onItemSelectedListener = this

        ArrayAdapter.createFromResource(
            this,
            R.array.languages_list,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
    }

    @RequiresApi(33)
    private fun setButtonOnCLick() {
        binding.goToSettingsButton.setOnClickListener {
            try {
                startActivity(Intent(Settings.ACTION_APP_LOCALE_SETTINGS, Uri.parse("package:$packageName")))
            } catch (e: ActivityNotFoundException) {
                Log.d(LOG_TAG, "No activity found to handle intent(ACTION_APP_LOCALE_SETTINGS)")
                Toast.makeText(this, R.string.cannot_open_per_app_locale_settings, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        Log.d(LOG_TAG, "IsFirst = $isFirst")
        if (isFirst) {
            isFirst = false
            return
        }
        Log.d(LOG_TAG, "Item clicked : ${parent.getItemAtPosition(pos)} | Position = $pos")
        when (pos) {
            0 -> setApplicationLocale(FRENCH_LANGUAGE_QUALIFIER_ANY_REGION)
            1 -> setApplicationLocale(ENGLISH_LANGUAGE_QUALIFIER_ANY_REGION)
        }
    }

    /** Call this on the main thread as it may require Activity.restart() **/
    private fun setApplicationLocale(listLanguage: String) {
        val appLocale: LocaleListCompat = LocaleListCompat.forLanguageTags(listLanguage)
        AppCompatDelegate.setApplicationLocales(appLocale)
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Do nothing
    }
}
