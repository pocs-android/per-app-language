# Per App Language

This POC aims to show how to change the language of the app without impacting the general language of the phone.

There are 2 ways:
1) For all versions of Android : You can change the language of application InApp (with **AppCompatDelegate**)
2) For Android versions >= 13 : You can redirect the user to the "Language Settings" page of the application (It's different from the phone global "Language Settings" page)

--> When running this application in Android < 13, we cannot access to the "Language Settings" page of the application.
--> When running this application in Android >= 13, we can access to the "Language Settings" page of the application.

Here are some references to this topic
- https://developer.android.com/guide/topics/resources/app-languages
- https://proandroiddev.com/exploring-the-new-android-13-per-app-language-preferences-8d99b971b578
- https://yggr.medium.com/exploring-android-13-per-app-language-preferences-f08a16e76657

